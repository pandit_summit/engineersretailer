package com.engineers.shopping.app.repository;

import com.engineers.shopping.app.model.ShippingAddress;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Maintains shipping address of user
 * @author pandit.summit@gmail.com
 */
@Repository
@Transactional
public interface ShippingAddressRepository extends CrudRepository<ShippingAddress, Integer> {

    @Query(" select shipping from ShippingAddress shipping inner join shipping.userId user where user.userId = :userId")
    public Iterable<ShippingAddress> findAddressbyShipping(@Param("userId") Integer id);
    
    @Query(" select shipping from ShippingAddress shipping inner join shipping.userId user where user.userId = :userId and shipping.shippingAddressId = :shipping ")
    public ShippingAddress findAddressbyShippingID(@Param("userId")int id, @Param("shipping") int shipping);
 }
