package com.engineers.shopping.app.controller;

import com.engineers.shopping.app.model.Orders;
import com.engineers.shopping.app.model.Reviews;
import com.engineers.shopping.app.model.Products;
import com.engineers.shopping.app.repository.ReviewRepository;
import com.engineers.shopping.app.repository.OrderRepository;
import com.engineers.shopping.app.repository.ProductRespository;

import io.swagger.annotations.*;
import static org.apache.commons.lang.StringUtils.isEmpty;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Maintains review for products and orders
 * @author Pandit_Biradar@gensler.com
 */

@RestController
@RequestMapping("/reviews")
@Api("Reviews Controller")
public class ReviewController {
    
    @Autowired
    private ProductRespository productRepository;
    
    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private ReviewRepository repository;
    
    @RequestMapping(method = GET, produces = "application/json")
    @ApiOperation(value = "View all reviews ",response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Iterable<Reviews> getCarts(Pageable pageable) {
       return repository.findAll(pageable); 
    }
    
    @RequestMapping(method = GET, value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Search a reviews with an reviews ID",response = Reviews.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved by id"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Reviews getCart(@PathVariable("id") int id) {
       return repository.findOne(id); 
    }
    
    @RequestMapping(method = POST, produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Create a new reviews")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New reviews created"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Order Cart"),
           })
    public ResponseEntity createCart(@RequestBody Reviews reviews) {
        
        if(reviews.getProductLinkId()!=null) {
            Products product = productRepository.findOne(reviews.getProductLinkId());
           if(product == null) return  new ResponseEntity("Product ID Misses",HttpStatus.GONE); 
            reviews.setProductId(product);
        }
        
        if(reviews.getOrderLinkId()!=null) {
            Orders order = orderRepository.findOne(reviews.getOrderLinkId());
           if(order == null) return  new ResponseEntity("Order ID Misses",HttpStatus.GONE); 
            reviews.setOrderId(order);
        }
        
        repository.save(reviews); 
        return new ResponseEntity("reviews saved successfully", HttpStatus.CREATED);
    }
    
    @RequestMapping(method = PUT, value = "/{id}", produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Update a reviews")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated  reviews"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the reviews"),
           })
    public ResponseEntity updateCart(@PathVariable("id") int id,
            @RequestBody Reviews reviews) {
        
        Reviews  result = repository.findOne(id);
        if(result == null) return  new ResponseEntity("Order Cart ID Misses",HttpStatus.GONE); 
        
        if(reviews.getProductLinkId()!=null) {
            Products product = productRepository.findOne(reviews.getProductLinkId());
           if(product == null) return  new ResponseEntity("Product ID Misses",HttpStatus.GONE); 
            result.setProductId(product);
        }
        
        if(reviews.getOrderLinkId()!=null) {
            Orders order = orderRepository.findOne(reviews.getOrderLinkId());
           if(order == null) return  new ResponseEntity("Order ID Misses",HttpStatus.GONE); 
            result.setOrderId(order);
        }
       if(isEmpty(reviews.getComments()))
           result.setComments(reviews.getComments());
       
       if(isEmpty(reviews.getStars()))
           result.setStars(reviews.getStars());
        
        repository.save(result); 
        return new ResponseEntity("Carts updated successfully", HttpStatus.ACCEPTED);
    }
    
    @RequestMapping(method = DELETE, value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Delete a reviews")
    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Delete  reviews"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the reviews"),
           })
    public ResponseEntity deleteCart(@PathVariable("id") int id) {
        Reviews  result = repository.findOne(id);
        
        if(result == null) return  new ResponseEntity("Reviews ID Misses",HttpStatus.GONE); 
        repository.delete(id);
        return new ResponseEntity("Reviews successfully", HttpStatus.ACCEPTED);
    }

}
