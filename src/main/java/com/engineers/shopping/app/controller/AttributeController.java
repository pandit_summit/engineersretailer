package com.engineers.shopping.app.controller;

import com.engineers.shopping.app.model.Attributes;
import com.engineers.shopping.app.model.Products;
import com.engineers.shopping.app.repository.AttributeRepository;
import com.engineers.shopping.app.repository.ProductRespository;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import static org.apache.commons.lang.StringUtils.isEmpty;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Maintains the Attributes specified by products
 * @author Pandit_Biradar@gensler.com
 */
@RestController
@RequestMapping("/{productId}/attributes")
@Api(value = "Attribute")
public class AttributeController {
    
    @Autowired
    private AttributeRepository repository;
    
    @Autowired
    private ProductRespository productRepository;
    
    @RequestMapping(method = GET, produces = "application/json")
    @ApiOperation(value = "View all Attributes specified by the product",response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Iterable<Attributes> getAttributes(@PathVariable("productId") int productId) {
       return repository.findProductAttributes(productId); 
    }
    
    @RequestMapping(method = GET, value = "/{attrId}", produces = "application/json")
    @ApiOperation(value = "Search a attribute id associate product",response = Attributes.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved by id"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Attributes getAttribute(@PathVariable("productId") int productId, 
            @PathVariable("attrId") int attrId) {
        
       return repository.findProductAttribute(productId,attrId); 
    }
    
    @RequestMapping(method = POST, produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Create a new Attributes")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New  Attributes created"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Attributes"),
           })
    public ResponseEntity createAttribute(@PathVariable("productId") int productId,
            @RequestBody Attributes attributes) {
        Products product = productRepository.findOne(productId);
        
        if(product == null) return new ResponseEntity("Product is missing",HttpStatus.GONE);
        
        attributes.setProduct(product);
        repository.save(attributes); 
       
        return new ResponseEntity("Attributes saved successfully", HttpStatus.CREATED);
    }
    
    @RequestMapping(method = PUT, value = "/{attrId}", produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Update a attributes")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated  attributes"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Attributes"),
           })
    public ResponseEntity updateAttribute(@PathVariable("productId") int productId,
            @PathVariable("attrId") int attrId,
            @RequestBody Attributes attributes) {
        
        Attributes result = repository.findProductAttribute(productId,attrId);
        
        if(result == null) return new ResponseEntity("Product is missing",HttpStatus.GONE);
        if(!isEmpty(attributes.getName())) 
            result.setName(attributes.getName());
        if(attributes.getValue()!=null)
            result.setValue(attributes.getValue());
                
        repository.save(attributes); 
        return new ResponseEntity("Attributes updated successfully", HttpStatus.ACCEPTED);
    }

}
