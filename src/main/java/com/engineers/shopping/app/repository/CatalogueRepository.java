package com.engineers.shopping.app.repository;

import com.engineers.shopping.app.model.Catalogue;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
/**
 * Maintains Catalogues 
 * @author pandit.summit@gmail.com
 */
@Repository
@Transactional
public interface CatalogueRepository extends CrudRepository<Catalogue, Integer> { }
