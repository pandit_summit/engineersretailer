package com.engineers.shopping.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.*;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pandit.summit@gmail.com
 */
@Entity
@Table(name = "attributes")
@XmlRootElement
public class Attributes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "attributes_id")
    private Integer attributesId;
    @Column(name = "name")
    private String name;
    @Column(name = "value")
    private Integer value;
    @JsonIgnore
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Products product;

    public Attributes() {
    }

    public Attributes(Integer attributesId) {
        this.attributesId = attributesId;
    }

    public Integer getAttributesId() {
        return attributesId;
    }

    public void setAttributesId(Integer attributesId) {
        this.attributesId = attributesId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
    
    @XmlTransient
    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (attributesId != null ? attributesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Attributes)) {
            return false;
        }
        Attributes other = (Attributes) object;
        if ((this.attributesId == null && other.attributesId != null) || (this.attributesId != null && !this.attributesId.equals(other.attributesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.engineers.shopping.model.Attributes[ attributesId=" + attributesId + " ]";
    }

}
