package com.engineers.shopping.app.controller;

import com.engineers.shopping.app.model.Users;
import com.engineers.shopping.app.repository.UserRepository;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;


/**
 * Maintain User endpoints
 * @author Pandit_Biradar@gensler.com
 */
@RestController
@RequestMapping("/users")
@Api(value = "Users")
public class UserController {
    
    @Autowired
    private UserRepository repository;
    
    @RequestMapping(method = GET, produces = "application/json")
    @ApiOperation(value = "View all Users",response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Iterable<Users> getUsers() {
       return repository.findAll(); 
    }
    
    @RequestMapping(method = GET, value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Search a User with an ID",response = Users.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved by id"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Users getUser(@PathVariable("id") int id) {
       return repository.findOne(id); 
    }
    
    @RequestMapping(method = POST, produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Create a new User")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New User created"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the User"),
           })
    public ResponseEntity createUser(@RequestBody Users user) {
       repository.save(user); 
       return new ResponseEntity("User saved successfully", HttpStatus.CREATED);
    }
    
    @RequestMapping(method = PUT, value = "/{id}", produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Update a User")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated  User"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the User"),
           })
    public ResponseEntity updateUser(@PathVariable("id") int id,@RequestBody Users user) {
        repository.save(user); 
       return new ResponseEntity("User updated successfully", HttpStatus.ACCEPTED);
    }

}
