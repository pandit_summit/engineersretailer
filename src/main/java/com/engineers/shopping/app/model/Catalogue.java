package com.engineers.shopping.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

/**
 * catalogue entity
 * @author pandit.summit@gmail.com
 */
@Entity
@Table(name = "catalogue")
@XmlRootElement
public class Catalogue implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "catalogue_id")
    private Integer catalogueId;
    @Column(name = "name")
    private String name;
    @Column(name = "status")
    private String status;
    @JsonIgnore
    @OneToMany(mappedBy = "catalogueId")
    private Collection<Products> products;

    public Catalogue() {
    }

    public Catalogue(Integer catalogueId) {
        this.catalogueId = catalogueId;
    }

    public Integer getCatalogueId() {
        return catalogueId;
    }

    public void setCatalogueId(Integer catalogueId) {
        this.catalogueId = catalogueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<Products> getProducts() {
        return products;
    }

    public void setProducts(Collection<Products> products) {
        this.products = products;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catalogueId != null ? catalogueId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Catalogue)) {
            return false;
        }
        Catalogue other = (Catalogue) object;
        if ((this.catalogueId == null && other.catalogueId != null) || (this.catalogueId != null && !this.catalogueId.equals(other.catalogueId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.engineers.shopping.model.Catalogue[ catalogueId=" + catalogueId + " ]";
    }

}
