package com.engineers.shopping.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.*;
import java.util.*;
import java.math.BigDecimal;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

/**
 *
 * @author pandit.summit@gmail.com
 */
@Entity
@Table(name = "orders")
@XmlRootElement
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "order_id")
    private Integer orderId;
    @Column(name = "order_number")
    private String orderNumber;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "total_price")
    private BigDecimal totalPrice;
    @Column(name = "status")
    private String status;
    @OneToMany(mappedBy = "orderId")
    @JsonIgnore
    private Collection<OrderCarts> orderCartsCollection;
    @OneToMany(mappedBy = "orderId")
    @JsonIgnore
    private Collection<OrderTracker> orderTrackerCollection;
    @OneToMany(mappedBy = "orderId")
    @JsonIgnore
    private Collection<Reviews> reviewsCollection;
    @JoinColumn(name = "shipping_address_id", referencedColumnName = "shipping_address_id")
    @ManyToOne(fetch=FetchType.LAZY)
    @JsonIgnore
    private ShippingAddress shippingAddressId;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id",nullable = false)
    @ManyToOne(fetch=FetchType.LAZY,optional = false)
    @JsonIgnore
    private Users userId;
    
    @Transient
    private Integer shippingLinkId;

    public Orders() {
    }

    public Orders(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<OrderCarts> getOrderCartsCollection() {
        return orderCartsCollection;
    }

    public void setOrderCartsCollection(Collection<OrderCarts> orderCartsCollection) {
        this.orderCartsCollection = orderCartsCollection;
    }

    @XmlTransient
    public Collection<OrderTracker> getOrderTrackerCollection() {
        return orderTrackerCollection;
    }

    public void setOrderTrackerCollection(Collection<OrderTracker> orderTrackerCollection) {
        this.orderTrackerCollection = orderTrackerCollection;
    }

    @XmlTransient
    public Collection<Reviews> getReviewsCollection() {
        return reviewsCollection;
    }

    public void setReviewsCollection(Collection<Reviews> reviewsCollection) {
        this.reviewsCollection = reviewsCollection;
    }

    public ShippingAddress getShippingAddressId() {
        return shippingAddressId;
    }

    public void setShippingAddressId(ShippingAddress shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }
    
    public Integer getShippingLinkId() {
       if(shippingAddressId!=null) 
           shippingLinkId = shippingAddressId.getShippingAddressId();
       return shippingLinkId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderId != null ? orderId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orders)) {
            return false;
        }
        Orders other = (Orders) object;
        if ((this.orderId == null && other.orderId != null) || (this.orderId != null && !this.orderId.equals(other.orderId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.engineers.shopping.model.Orders[ orderId=" + orderId + " ]";
    }

}
