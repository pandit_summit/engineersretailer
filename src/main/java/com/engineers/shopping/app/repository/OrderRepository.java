package com.engineers.shopping.app.repository;

import com.engineers.shopping.app.model.*;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author pandit.summit@gmail.com
 */
@Repository
@Transactional
public interface OrderRepository extends CrudRepository<Orders, Integer> {
    
    @Query(" select o from Orders o inner join o.userId user where user.userId = :userId ")
    public Iterable<Orders> findUserbyOrderId(@Param("userId") Integer id);
    @Query(" select o from Orders o inner join o.userId user where user.userId = :userId and o.orderId = :orderID ")
    public Orders findByUserIdAndOrderId(@Param("userId") Integer id, @Param("orderID") Integer orderID);

}
