package com.engineers.shopping.app.controller;


import com.engineers.shopping.app.model.CouponHistory;
import com.engineers.shopping.app.repository.CouponHistoryRepository;
import io.swagger.annotations.*;
import static org.apache.commons.lang.StringUtils.isEmpty;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;
/**
 * Maintains the coupon history for associated order number
 * @author Pandit_Biradar@gensler.com
 */
@RestController
@RequestMapping("/coupon")
@Api("Coupon History")
public class CouponHistoryController {
    
    @Autowired
    private CouponHistoryRepository repository;
    
    @RequestMapping(method = GET, produces = "application/json")
    @ApiOperation(value = "View all coupons ",response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Iterable<CouponHistory> getCoupons(Pageable pageable) {
       return repository.findAll(pageable); 
    }
    
    @RequestMapping(method = GET, value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Search a coupons with an coupons ID",response = CouponHistory.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved by id"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody CouponHistory getCoupon(@PathVariable("id") int id) {
       return repository.findOne(id); 
    }
    
    @RequestMapping(method = POST, produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Create a new coupons")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New  coupon histroy created"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Order Cart"),
           })
    public ResponseEntity createCoupon(@RequestBody CouponHistory coupon) {
        repository.save(coupon); 
        return new ResponseEntity("coupon histroy saved successfully", HttpStatus.CREATED);
    }
    
    @RequestMapping(method = PUT, value = "/{id}", produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Update a coupon histroy")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated  coupon histroy"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the coupon histroy"),
           })
    public ResponseEntity updateCoupon(@PathVariable("id") int id,
            @RequestBody CouponHistory coupon) {
        
        CouponHistory result = repository.findOne(id);
        if(result == null) return  new ResponseEntity("CouponHistory ID Misses",HttpStatus.GONE); 
        
        if(isEmpty(coupon.getCouponHistory())) 
             result.setCouponHistory(coupon.getCouponHistory());
        if(isEmpty(coupon.getOrderNumber())) 
         result.setOrderNumber(coupon.getOrderNumber());
        if(isEmpty(coupon.getAppliedDiscountValue()))
             result.setAppliedDiscountValue(coupon.getAppliedDiscountValue());
          
        repository.save(result); 
        return new ResponseEntity("CouponHistory updated successfully", HttpStatus.ACCEPTED);
    }
    
    @RequestMapping(method = DELETE, value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Delete a CouponHistory")
    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Delete  CouponHistory"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the CouponHistory"),
           })
    public ResponseEntity deleteCoupon(@PathVariable("id") int id) {
        CouponHistory result = repository.findOne(id);
        
        if(result == null) return  new ResponseEntity("CouponHistory ID Misses",HttpStatus.GONE); 
        repository.delete(id);
        return new ResponseEntity("CouponHistory deleted successfully", HttpStatus.ACCEPTED);
    }

}
