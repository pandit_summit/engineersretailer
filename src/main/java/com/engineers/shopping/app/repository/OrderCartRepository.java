package com.engineers.shopping.app.repository;

import com.engineers.shopping.app.model.OrderCarts;

import javax.transaction.Transactional;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Maintains Order carts
 * @author pandit.summit@gmail.com
 */
@Repository
@Transactional
public interface OrderCartRepository extends PagingAndSortingRepository<OrderCarts, Integer> { }
