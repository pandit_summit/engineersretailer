package com.engineers.shopping.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.*;
import java.math.BigDecimal;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

/**
 * Order Cart entity 
 * @author pandit.summit@gmail.com
 */
@Entity
@Table(name = "order_carts")
@XmlRootElement
public class OrderCarts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "order_cart_id")
    private Integer orderCartId;
    @Column(name = "unit_type")
    private String unitType;
    @Column(name = "units")
    private Integer units;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private BigDecimal price;
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    @ManyToOne(fetch=FetchType.LAZY)
    @JsonIgnore
    private Orders orderId;
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    @JsonIgnore
    @ManyToOne(fetch=FetchType.LAZY)
    private Products productId;
    
    @Transient
    private Integer productLinkId;
    
    @Transient
    private Integer orderLinkId;

    public OrderCarts() {
    }

    public OrderCarts(Integer orderCartId) {
        this.orderCartId = orderCartId;
    }

    public Integer getOrderCartId() {
        return orderCartId;
    }

    public void setOrderCartId(Integer orderCartId) {
        this.orderCartId = orderCartId;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    
    @XmlTransient
    public Orders getOrderId() {
        return orderId;
    }

    public void setOrderId(Orders orderId) {
        this.orderId = orderId;
    }
    
    @XmlTransient
    public Products getProductId() {
        return productId;
    }

    public void setProductId(Products productId) {
        this.productId = productId;
    }
    
    public Integer getProductLinkId() {
        if(productId!=null)
            return productLinkId = productId.getProductId();
        return productLinkId;
    }
    
    public Integer getOrderLinkId() {
        if(orderId!=null) 
            return orderLinkId =  orderId.getOrderId();
        return orderLinkId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderCartId != null ? orderCartId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderCarts)) {
            return false;
        }
        OrderCarts other = (OrderCarts) object;
        if ((this.orderCartId == null && other.orderCartId != null) || (this.orderCartId != null && !this.orderCartId.equals(other.orderCartId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.engineers.shopping.model.OrderCarts[ orderCartId=" + orderCartId + " ]";
    }
}
