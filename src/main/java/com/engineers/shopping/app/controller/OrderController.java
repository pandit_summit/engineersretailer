package com.engineers.shopping.app.controller;

import com.engineers.shopping.app.model.*;
import com.engineers.shopping.app.repository.*;

import io.swagger.annotations.*;
import static org.apache.commons.lang.StringUtils.isEmpty;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;
/**
 * Maintains the orders specified by user
 * @author Pandit_Biradar@gensler.com
 */
@RestController
@RequestMapping("/{id}/orders")
@Api(value = "Orders")
public class OrderController {
    
    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private ShippingAddressRepository shippingRespository;
    
    @RequestMapping(method = GET, produces = "application/json")
    @ApiOperation(value = "View all Orders for User",response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Iterable<Orders> getOrders(@PathVariable("id") Integer id) {
       return orderRepository.findUserbyOrderId(id); 
    }
    
    @RequestMapping(method = GET, value = "/{orderID}", produces = "application/json")
    @ApiOperation(value = "Search a Orders with an Order ID and User Id",response = Orders.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved by id"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Orders getOrder(@PathVariable("id") int id,
            @PathVariable("orderID") int orderID) {
        
       return orderRepository.findByUserIdAndOrderId(id,orderID); 
    }
    
    @RequestMapping(method = POST, produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Create a new Order")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New User Order created"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the User"),
           })
    public ResponseEntity createUserOrder(@PathVariable("id") int id,
            @RequestBody Orders orders) {
        
        Users user = userRepository.findOne(id);
        if(user == null) return new ResponseEntity("User is missing",HttpStatus.GONE);
        orders.setUserId(user);
        
        ShippingAddress shipping = shippingRespository.findOne(orders.getShippingLinkId());
        if(shipping == null) return new ResponseEntity("User shipping Address is missing",HttpStatus.GONE);
        orders.setShippingAddressId(shipping);
        
        orderRepository.save(orders); 
        return new ResponseEntity("Order saved successfully", HttpStatus.CREATED);
    }
    
    @RequestMapping(method = PUT, value = "/{orderID}", produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Update a Order")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated  User Order"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the User"),
           })
    public ResponseEntity updateUserOrder(@PathVariable("id") int id,
            @PathVariable("orderID") int orderID,
            @RequestBody Orders order) {
        
        Orders result = orderRepository.findByUserIdAndOrderId(id,orderID);
        if(result == null) return new ResponseEntity("User Order is missing",HttpStatus.GONE);
        
        ShippingAddress shipping = shippingRespository.findOne(order.getShippingLinkId());
        if(shipping == null) return new ResponseEntity("User shipping Address is missing",HttpStatus.GONE);
        order.setShippingAddressId(shipping);
        
        if(!isEmpty(order.getStatus())) {
            result.setStatus(order.getStatus());
        }
        
        if(order.getTotalPrice()!=null) {
            result.setTotalPrice(order.getTotalPrice());
        }
        
        orderRepository.save(result); 
        return new ResponseEntity("User Order updated successfully", HttpStatus.ACCEPTED);
    }

}
