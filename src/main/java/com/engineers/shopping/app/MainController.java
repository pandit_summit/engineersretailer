package com.engineers.shopping.app;

import java.util.concurrent.TimeUnit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;

import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Starts up with the configured Spring MVC web services using Spring Boot.
 * @author pandit.summit@gmail.com
 */
@SpringBootApplication(exclude = RepositoryRestMvcAutoConfiguration.class)
public class MainController {
  
    public static void main(String[] args) {
        SpringApplication.run(MainController.class, args);
    }
    
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                   .allowedOrigins("*") //
                  .allowedMethods("OPTIONS", "HEAD", "GET", "PUT", "POST", "DELETE", "PATCH")
                  .allowedHeaders("*") //
                  .exposedHeaders("WWW-Authenticate")
                  .allowCredentials(true)
                  .maxAge(TimeUnit.DAYS.toSeconds(1));
            }
        };
    }
    
}
