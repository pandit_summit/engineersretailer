package com.engineers.shopping.app.controller;

import com.engineers.shopping.app.model.Catalogue;
import com.engineers.shopping.app.model.Products;
import com.engineers.shopping.app.repository.CatalogueRepository;
import com.engineers.shopping.app.repository.ProductRespository;

import io.swagger.annotations.*;
import static org.apache.commons.lang.StringUtils.isEmpty;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Maintain Product Details
 * @author Pandit_Biradar@gensler.com
 */
@RestController
@RequestMapping("/products")
@Api(value = "Product")
public class ProductController {
    
    @Autowired
    private ProductRespository repository;
    
    @Autowired
    private CatalogueRepository cataLogueRepository;
    
    @RequestMapping(method = GET, produces = "application/json")
    @ApiOperation(value = "View all products ",response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Iterable<Products> getProducts(@RequestParam(value = "name",required=false) String name,Pageable page) {
        if(!isEmpty(name))
        return repository.findByName(name,page);
       return repository.findAll(page); 
    }
    
    @RequestMapping(method = GET, value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Search a Products with an Products ID",response = Products.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved by id"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Products getProduct(@PathVariable("id") int id) {
       return repository.findOne(id); 
    }
    
    @RequestMapping(method = POST, produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Create a new Products")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New  Product created"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Product"),
           })
    public ResponseEntity createProduct(@RequestBody Products products) {
        
        if(products.getCatalogueLinkId()!=null) {
            Catalogue catalogue = cataLogueRepository.findOne(products.getCatalogueLinkId());
           if(catalogue == null) return  new ResponseEntity("Catalogue ID Misses",HttpStatus.GONE); 
            products.setCatalogueId(catalogue);
        }
        
        repository.save(products); 
        return new ResponseEntity("Product saved successfully", HttpStatus.CREATED);
    }
    
    @RequestMapping(method = PUT, value = "/{id}", produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Update a Product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated  Product"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Product"),
           })
    public ResponseEntity updateProduct(@PathVariable("id") int id,
            @RequestBody Products products) {
        Products result = repository.findOne(id);
        
        if(products.getCatalogueLinkId()!=null) {
            Catalogue catalogue = cataLogueRepository.findOne(products.getCatalogueLinkId());
           if(catalogue == null) return  new ResponseEntity("Catalogue ID Misses",HttpStatus.GONE); 
            result.setCatalogueId(catalogue);
        }
        
        if(!isEmpty(products.getName())) result.setName(products.getName());
        if(products.getEnteredDate()!=null) result.setEnteredDate(products.getEnteredDate());
        if(!isEmpty(products.getReviewStars())) result.setReviewStars(products.getReviewStars());
        
        repository.save(result); 
        return new ResponseEntity("Product updated successfully", HttpStatus.ACCEPTED);
    }
    
}
