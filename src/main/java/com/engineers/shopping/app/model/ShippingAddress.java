package com.engineers.shopping.app.model;

import java.io.*;
import java.util.*;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

/**
 *
 * @author pandit.summit@gmail.com
 */
@Entity
@Table(name = "shipping_address")
@XmlRootElement
public class ShippingAddress implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "shipping_address_id")
    private Integer shippingAddressId;
    @Column(name = "address")
    private String address;
    @Column(name = "pin_code")
    private String pinCode;
    @Column(name = "state")
    private String state;
    @Column(name = "country")
    private String country;
    @OneToMany(mappedBy = "shippingAddressId")
    private Collection<Orders> ordersCollection;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Users userId;

    public ShippingAddress() {
    }

    public ShippingAddress(Integer shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }

    public Integer getShippingAddressId() {
        return shippingAddressId;
    }

    public void setShippingAddressId(Integer shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @XmlTransient
    public Collection<Orders> getOrdersCollection() {
        return ordersCollection;
    }

    public void setOrdersCollection(Collection<Orders> ordersCollection) {
        this.ordersCollection = ordersCollection;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shippingAddressId != null ? shippingAddressId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ShippingAddress)) {
            return false;
        }
        ShippingAddress other = (ShippingAddress) object;
        if ((this.shippingAddressId == null && other.shippingAddressId != null) || (this.shippingAddressId != null && !this.shippingAddressId.equals(other.shippingAddressId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.engineers.shopping.model.ShippingAddress[ shippingAddressId=" + shippingAddressId + " ]";
    }

}
