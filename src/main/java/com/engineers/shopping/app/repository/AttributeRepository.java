package com.engineers.shopping.app.repository;

import com.engineers.shopping.app.model.Attributes;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Maintains attributes 
 * @author pandit.summit@gmail.com
 */
@Repository
@Transactional
public interface AttributeRepository extends CrudRepository<Attributes, Integer> {
    
    @Query(" select att from Attributes att inner join  att.product prod where prod.productId = :productId")
    public Iterable<Attributes> findProductAttributes(@Param("productId") Integer productId);
    
    @Query(" select att from Attributes att inner join  att.product prod where prod.productId = :productId and att.attributesId =:attrId")
    public Attributes findProductAttribute(@Param("productId") Integer productId, @Param("attrId") Integer attrId);
   

}
