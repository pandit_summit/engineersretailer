package com.engineers.shopping.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

/**
 * Products Entity
 * @author pandit.summit@gmail.com
 */
@Entity
@Table(name = "products")
@XmlRootElement
public class Products implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "product_id")
    private Integer productId;
    @Column(name = "name")
    private String name;
    @Column(name = "entered_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    @Column(name = "status")
    private String status;
    @Column(name = "review_stars")
    private String reviewStars;
    @OneToMany(mappedBy = "productId")
    @JsonIgnore
    private Collection<OrderCarts> orderCarts;
    @OneToMany(mappedBy = "productId")
    @JsonIgnore
    private Collection<Reviews> reviews;
    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private Collection<Attributes> attributes;
    @JoinColumn(name = "catalogue_id", referencedColumnName = "catalogue_id")
    @ManyToOne(fetch=FetchType.LAZY)
    @JsonIgnore
    private Catalogue catalogueId;
    @Transient
    private Integer catalogueLinkId;
    public Products() {
    }

    public Products(Integer productId) {
        this.productId = productId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReviewStars() {
        return reviewStars;
    }

    public void setReviewStars(String reviewStars) {
        this.reviewStars = reviewStars;
    }

    @XmlTransient
    public Collection<OrderCarts> getOrderCarts() {
        return orderCarts;
    }

    public void setOrderCarts(Collection<OrderCarts> orderCarts) {
        this.orderCarts = orderCarts;
    }

    @XmlTransient
    public Collection<Reviews> getReviews() {
        return reviews;
    }

    public void setReviews(Collection<Reviews> reviews) {
        this.reviews = reviews;
    }

    @XmlTransient
    public Collection<Attributes> getAttributes() {
        return attributes;
    }

    public void setAttributes(Collection<Attributes> attributes) {
        this.attributes = attributes;
    }
    
    @XmlTransient
    public Catalogue getCatalogueId() {
        return catalogueId;
    }

    public void setCatalogueId(Catalogue catalogueId) {
        this.catalogueId = catalogueId;
    }
    
    public Integer getCatalogueLinkId() {
        if(catalogueId!=null) catalogueLinkId = catalogueId.getCatalogueId();
        return catalogueLinkId;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productId != null ? productId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Products)) {
            return false;
        }
        Products other = (Products) object;
        if ((this.productId == null && other.productId != null) || (this.productId != null && !this.productId.equals(other.productId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.engineers.shopping.model.Products[ productId=" + productId + " ]";
    }

}
