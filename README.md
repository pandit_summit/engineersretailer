## Engineer Retailer Service Developer Tools

## Overview

The following discussions cover tooling we use for source code, code editing, testing, compilation, library dependency management,
documentation, builds, artifact deployment, and project management. The following development tools are either required, strongly recommended.

## Java JDK 8

Download the JDK 8 for your respective operating system.
link : http://www.oracle.com/technetwork/java/javase/downloads/index.html

## Gradle (required for  builds) 

Gradle we are using for build your project life cycle , please download the Gradle and setup in your machine without this you cant
 start your project.
  
installation on windows :

Step 1: first verify if java is installed in command prompt

C:\> java - version
If the command is executed successfully, you will get the following output.

java version "1.8.0_66"
Java(TM) SE Runtime Environment (build 1.8.0_66-b18)
Java HotSpot(TM) 64-Bit Server VM (build 25.66-b18, mixed mode)

Step 2 − Download Gradle Build File
Download the latest version of Gradle from the Download Gradle link: https://gradle.org/install/

Step 3 − Set Up Environment for Gradle
Extract the downloaded zip file named gradle-2.11-all.zip and copy the distribution files from Downloads\gradle-2.11\ to C:\gradle\ location.

Later, add the C:\gradle and C:\gradle\bin directories to the GRADLE_HOME and PATH system variables. Right-click on My Computer → Click properties → Advanced system settings → Environment variables. There you will find a dialog box for creating and editing system variables. Click ‘New’ button for creating GRADLE_HOME variable (follow the left side screenshot). Click ‘Edit’ for editing the existing Path system variable (follow the right side screenshot). The process is shown in the following screenshots.

Setup Environment for Gradle

for installation in other operation system you can follow the link : https://gradle.org/install/

## BitBucket(Source code versioning)

Singup BitBucket account with you email Id , and get the access for your code repository.

link : https://bitbucket.org

get the repo access from admin.

and Download the git in your system to do the git 

link : https://git-scm.com/download/win

## IDE Tool

Download Eclipse or Netbean for your IDE purpose 

Eclipse link : https://www.eclipse.org/downloads/?
Netbean link : https://netbeans.org/downloads/

## Mysql(Sql Server)

Download the MySQL and install in your machine 
link : https://www.mysql.com/downloads/


## Build Project

open the command prompt Go to the your project root folder and run below commands 
gradle clean build (if build is success)
then run
gradle bootRun

better add the plugins in your IDE Eclipse and Netbeans

## Rest API Document

You can test and see the Rest API Document 
After running the 
gradle bootRun
open the below link browser
http://localhost:8080/rest-Service.Shopping/api/swagger-ui.html

you can see all REST API's endpoint and CRUD operations



