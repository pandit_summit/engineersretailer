package com.engineers.shopping.app.repository;

import com.engineers.shopping.app.model.Reviews;
import javax.transaction.Transactional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Maintains Reviews for an order
 * @author pandit.summit@gmail.com
 */
@Repository
@Transactional
public interface ReviewRepository extends PagingAndSortingRepository<Reviews, Integer> { }
