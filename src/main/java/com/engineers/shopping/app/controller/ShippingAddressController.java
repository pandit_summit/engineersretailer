package com.engineers.shopping.app.controller;

import com.engineers.shopping.app.model.ShippingAddress;
import com.engineers.shopping.app.model.Users;
import com.engineers.shopping.app.repository.ShippingAddressRepository;
import com.engineers.shopping.app.repository.UserRepository;

import io.swagger.annotations.*;
import static org.apache.commons.lang.StringUtils.isEmpty;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Shipping address associated with user
 * @author Pandit_Biradar@gensler.com
 */
@RestController
@RequestMapping("/{id}/shippingAddress")
@Api(value = "ShippingAddress")
public class ShippingAddressController {
    
    @Autowired
    private ShippingAddressRepository repository;
    @Autowired
    private UserRepository userRepository;
    
    @RequestMapping(method = GET, produces = "application/json")
    @ApiOperation(value = "View all Shipping address for User",response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Iterable<ShippingAddress> getShippingAddress(@PathVariable("id") Integer id) {
       return repository.findAddressbyShipping(id); 
    }
    
    @RequestMapping(method = GET, value = "/{shipping}", produces = "application/json")
    @ApiOperation(value = "Search a Shipping Address with an ShppingAddress ID and User Id",response = ShippingAddress.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved by id"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody ShippingAddress getShippingAddress(@PathVariable("id") int id,
            @PathVariable("shipping") int shipping) {
        
       return repository.findAddressbyShippingID(id,shipping); 
    }
    
    @RequestMapping(method = POST, produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Create a new Address")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New User Shipping Address created"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the User Shipping"),
           })
    public ResponseEntity createShippingAddress(@PathVariable("id") int id,
            @RequestBody ShippingAddress shippingAddress) {
        
        Users user = userRepository.findOne(id);
        if(user == null) return new ResponseEntity("User is missing",HttpStatus.GONE);
        
        shippingAddress.setUserId(user);
        repository.save(shippingAddress); 
       
        return new ResponseEntity("User Shipping Address saved successfully", HttpStatus.CREATED);
    }
    
    @RequestMapping(method = PUT, value = "/{shipping}", produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Update a shipping address")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated  User shipping Address"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the User"),
           })
    public ResponseEntity updateUserOrder(@PathVariable("id") int id,
            @PathVariable("shipping") int shipping,
            @RequestBody ShippingAddress shippingAddress) {
        
        ShippingAddress result = repository.findAddressbyShippingID(id,shipping);
        if(result == null) return new ResponseEntity("User  shipping Address is missing",HttpStatus.GONE);
        
        if(!isEmpty(shippingAddress.getAddress())) {
            result.setAddress(shippingAddress.getAddress());
        }
        
        if(!isEmpty(shippingAddress.getCountry())) {
            result.setCountry(shippingAddress.getCountry());
        }
        
        if(!isEmpty(shippingAddress.getPinCode())) {
            result.setPinCode(shippingAddress.getPinCode());
        }
        
        if(!isEmpty(shippingAddress.getState())) {
            result.setState(shippingAddress.getState());
        }
        
        repository.save(result); 
        return new ResponseEntity("User Shipping Address updated successfully", HttpStatus.ACCEPTED);
    }

}
