package com.engineers.shopping.app.repository;

import com.engineers.shopping.app.model.Orders;
import com.engineers.shopping.app.model.Transaction;

import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Maintains the transaction history of user
 * @author pandit.summit@gmail.com
 */
@Repository
@Transactional
public interface TransactionRepository extends PagingAndSortingRepository<Transaction, Integer> {
   
    @Query(" select trans from Transaction trans inner join trans.userId user where user.userId = :userId")
    public Page<Transaction> findTransbyUserId(@Param("userId") Integer id, Pageable page);
    
    @Query(" select trans from Transaction trans inner join trans.userId user where user.userId = :userId and trans.transactionId= :transId")
    public Transaction findTransbyId(@Param("userId") Integer id, @Param("transId") Integer transId);
 }
