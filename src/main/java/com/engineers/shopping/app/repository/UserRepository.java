package com.engineers.shopping.app.repository;

import com.engineers.shopping.app.model.Users;

import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Maintains User data
 * @author pandit.summit@gmail.com
 */
@Repository
@Transactional
public interface UserRepository extends CrudRepository<Users, Integer> {
}
