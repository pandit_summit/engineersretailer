package com.engineers.shopping.app.repository;

import com.engineers.shopping.app.model.OrderTracker;

import javax.transaction.Transactional;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Maintains order tracker
 * @author pandit.summit@gmail.com
 */
@Repository
@Transactional
public interface OrderTrackerRepository extends PagingAndSortingRepository<OrderTracker, Integer> { }
