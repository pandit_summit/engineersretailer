package com.engineers.shopping.app.model;

import java.io.*;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

/**
 * Reviews entity id
 * @author pandit.summit@gmail.com
 */
@Entity
@Table(name = "reviews")
@XmlRootElement
public class Reviews implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "review_id")
    private Integer reviewId;
    @Column(name = "stars")
    private String stars;
    @Column(name = "comments")
    private String comments;
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Orders orderId;
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Products productId;
    
    @Transient
    private Integer productLinkId;
    
    @Transient
    private Integer orderLinkId;
    
    public Reviews() {
    }

    public Reviews(Integer reviewId) {
        this.reviewId = reviewId;
    }

    public Integer getReviewId() {
        return reviewId;
    }

    public void setReviewId(Integer reviewId) {
        this.reviewId = reviewId;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
    
    @XmlTransient
    public Orders getOrderId() {
        return orderId;
    }

    public void setOrderId(Orders orderId) {
        this.orderId = orderId;
    }
    
    @XmlTransient
    public Products getProductId() {
        return productId;
    }

    public void setProductId(Products productId) {
        this.productId = productId;
    }
    
    public Integer getProductLinkId() {
        return productLinkId;
    }

    public void setProductLinkId(Integer productLinkId) {
        this.productLinkId = productLinkId;
    }

    public Integer getOrderLinkId() {
        return orderLinkId;
    }

    public void setOrderLinkId(Integer orderLinkId) {
        this.orderLinkId = orderLinkId;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reviewId != null ? reviewId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reviews)) {
            return false;
        }
        Reviews other = (Reviews) object;
        if ((this.reviewId == null && other.reviewId != null) || (this.reviewId != null && !this.reviewId.equals(other.reviewId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.engineers.shopping.model.Reviews[ reviewId=" + reviewId + " ]";
    }
}
