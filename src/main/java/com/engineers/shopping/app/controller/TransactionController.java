package com.engineers.shopping.app.controller;

import com.engineers.shopping.app.model.*;
import com.engineers.shopping.app.repository.*;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Maintains transaction history of user
 * @author Pandit_Biradar@gensler.com
 */
@RestController
@RequestMapping("/{id}/transaction")
public class TransactionController {
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private TransactionRepository repository;
    
    @RequestMapping(method = GET, produces = "application/json")
    @ApiOperation(value = "View all Transaction for User",response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Iterable<Transaction> getTransactions(@PathVariable("id") Integer id, Pageable page) {
       return repository.findTransbyUserId(id, page); 
    }
    
    @RequestMapping(method = GET, value = "/{transId}", produces = "application/json")
    @ApiOperation(value = "Search a Transaction with an Transaction ID and User Id",response = Transaction.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved by id"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Transaction getTransaction(@PathVariable("id") int id,
            @PathVariable("transId") int transId) {
        
       return repository.findTransbyId(id,transId); 
    }
    
    @RequestMapping(method = POST, produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Create a new transaction")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New User transaction created"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the transaction"),
           })
    public ResponseEntity createUserOrder(@PathVariable("id") int id,
            @RequestBody Transaction trans) {
        
        Users user = userRepository.findOne(id);
        if(user == null) return new ResponseEntity("User is missing",HttpStatus.GONE);
        trans.setUserId(user);
        
        repository.save(trans); 
        return new ResponseEntity("transaction saved successfully", HttpStatus.CREATED);
    }

}
