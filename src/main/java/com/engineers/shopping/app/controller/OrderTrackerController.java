package com.engineers.shopping.app.controller;


import com.engineers.shopping.app.model.OrderTracker;
import com.engineers.shopping.app.model.Orders;
import com.engineers.shopping.app.repository.OrderRepository;
import com.engineers.shopping.app.repository.OrderTrackerRepository;

import io.swagger.annotations.*;
import static org.apache.commons.lang.StringUtils.isEmpty;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Maintains order tracker for associated order
 * @author Pandit_Biradar@gensler.com
 */
@RestController
@RequestMapping("/trackers")
@Api("Order Trackers")
public class OrderTrackerController {
    
    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private OrderTrackerRepository repository;
    
    @RequestMapping(method = GET, produces = "application/json")
    @ApiOperation(value = "View all order tracks ",response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Iterable<OrderTracker> getTrackers(Pageable pageable) {
       return repository.findAll(pageable); 
    }
    
    @RequestMapping(method = GET, value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Search a order tracks with an tracks ID",response = OrderTracker.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved by id"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody OrderTracker getTracker(@PathVariable("id") int id) {
       return repository.findOne(id); 
    }
    
    @RequestMapping(method = POST, produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Create a new Order Tracker")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New  Order Tracker created"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Order tracker"),
           })
    public ResponseEntity createTracker(@RequestBody OrderTracker tracker) {
        
        if(tracker.getOrderLinkId()!=null) {
            Orders order = orderRepository.findOne(tracker.getOrderLinkId());
           if(order == null) return  new ResponseEntity("Order ID Misses",HttpStatus.GONE); 
            tracker.setOrderId(order);
        }
        
        repository.save(tracker); 
        return new ResponseEntity("Order tracker saved successfully", HttpStatus.CREATED);
    }
    
    @RequestMapping(method = PUT, value = "/{id}", produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Update a Order tracker")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated  Order tracker"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Order tracker"),
           })
    public ResponseEntity updateCart(@PathVariable("id") int id,
            @RequestBody OrderTracker tracker) {
        
        OrderTracker result = repository.findOne(id);
        if(result == null) return  new ResponseEntity("Order Tracker ID Misses",HttpStatus.GONE); 
        
        if(tracker.getOrderLinkId()!=null) {
            Orders order = orderRepository.findOne(tracker.getOrderLinkId());
           if(order == null) return  new ResponseEntity("Order ID Misses",HttpStatus.GONE); 
            result.setOrderId(order);
        }
        
        if(!isEmpty(tracker.getState()))
            result.setNotes(tracker.getState());
        
        if(!isEmpty(tracker.getNotes()))
            result.setNotes(tracker.getNotes());
        
         if(tracker.getStartDate()!=null)
            result.setStartDate(tracker.getStartDate());
        
        if(tracker.getEndDate()!=null)
            result.setEndDate(tracker.getEndDate());
        
        repository.save(result); 
        return new ResponseEntity("tracker updated successfully", HttpStatus.ACCEPTED);
    }
    
    @RequestMapping(method = DELETE, value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Delete a Order tracker")
    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Delete  Order Cart"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Order tracker"),
           })
    public ResponseEntity deleteCart(@PathVariable("id") int id) {
        OrderTracker result = repository.findOne(id);
        
        if(result == null) return  new ResponseEntity("Order tracker ID Misses",HttpStatus.GONE); 
        repository.delete(id);
        return new ResponseEntity("tracker deleted successfully", HttpStatus.ACCEPTED);
    }
    
}
