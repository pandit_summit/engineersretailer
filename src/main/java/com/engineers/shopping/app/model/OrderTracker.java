package com.engineers.shopping.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.*;
import java.util.*;
import javax.persistence.*;
import javax.xml.bind.annotation.*;

/**
 * Order Tracker Entity
 * @author pandit.summit@gmail.com
 */
@Entity
@Table(name = "order_tracker")
@XmlRootElement
public class OrderTracker implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "order_tracker_id")
    private Integer orderTrackerId;
    @Column(name = "state")
    private String state;
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "notes")
    private String notes;
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    @ManyToOne(fetch=FetchType.LAZY)
    @JsonIgnore
    private Orders orderId;
    
    @Transient
    private Integer orderLinkId;

    public OrderTracker() {
    }

    public OrderTracker(Integer orderTrackerId) {
        this.orderTrackerId = orderTrackerId;
    }

    public Integer getOrderTrackerId() {
        return orderTrackerId;
    }

    public void setOrderTrackerId(Integer orderTrackerId) {
        this.orderTrackerId = orderTrackerId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    
    @XmlTransient
    public Orders getOrderId() {
        return orderId;
    }
    
    public Integer getOrderLinkId() {
        if(orderId!=null) 
            orderLinkId = orderId.getOrderId();
        return orderLinkId;
    }

    public void setOrderId(Orders orderId) {
        this.orderId = orderId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderTrackerId != null ? orderTrackerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderTracker)) {
            return false;
        }
        OrderTracker other = (OrderTracker) object;
        if ((this.orderTrackerId == null && other.orderTrackerId != null) || (this.orderTrackerId != null && !this.orderTrackerId.equals(other.orderTrackerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.engineers.shopping.model.OrderTracker[ orderTrackerId=" + orderTrackerId + " ]";
    }

}
