package com.engineers.shopping.app.repository;

import com.engineers.shopping.app.model.CouponHistory;
import javax.transaction.Transactional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Maintain Coupon history 
 * @author pandit.summit@gmail.com
 */
@Repository
@Transactional
public interface CouponHistoryRepository extends PagingAndSortingRepository<CouponHistory, Integer> { }
