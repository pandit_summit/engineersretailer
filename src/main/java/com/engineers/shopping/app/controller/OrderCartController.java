package com.engineers.shopping.app.controller;


import com.engineers.shopping.app.model.Orders;
import com.engineers.shopping.app.model.OrderCarts;
import com.engineers.shopping.app.model.Products;
import com.engineers.shopping.app.repository.OrderCartRepository;
import com.engineers.shopping.app.repository.OrderRepository;
import com.engineers.shopping.app.repository.ProductRespository;

import io.swagger.annotations.*;
import static org.apache.commons.lang.StringUtils.isEmpty;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Maintains the order carts 
 * @author Pandit_Biradar@gensler.com
 */
@RestController
@RequestMapping("/carts")
@Api(value = "Order Carts")
public class OrderCartController {
    
    @Autowired
    private ProductRespository productRepository;
    
    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private OrderCartRepository repository;
    
    @RequestMapping(method = GET, produces = "application/json")
    @ApiOperation(value = "View all carts ",response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Iterable<OrderCarts> getCarts(Pageable pageable) {
       return repository.findAll(pageable); 
    }
    
    @RequestMapping(method = GET, value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Search a carts with an cart ID",response = OrderCarts.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved by id"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody OrderCarts getCart(@PathVariable("id") int id) {
       return repository.findOne(id); 
    }
    
    @RequestMapping(method = POST, produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Create a new Order cart")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New  Order cart created"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Order Cart"),
           })
    public ResponseEntity createCart(@RequestBody OrderCarts carts) {
        
        if(carts.getProductLinkId()!=null) {
            Products product = productRepository.findOne(carts.getProductLinkId());
           if(product == null) return  new ResponseEntity("Product ID Misses",HttpStatus.GONE); 
            carts.setProductId(product);
        }
        
        if(carts.getOrderLinkId()!=null) {
            Orders order = orderRepository.findOne(carts.getOrderLinkId());
           if(order == null) return  new ResponseEntity("Order ID Misses",HttpStatus.GONE); 
            carts.setOrderId(order);
        }
        
        repository.save(carts); 
        return new ResponseEntity("Order cart saved successfully", HttpStatus.CREATED);
    }
    
    @RequestMapping(method = PUT, value = "/{id}", produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Update a Order cart")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated  Order Cart"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Order cart"),
           })
    public ResponseEntity updateCart(@PathVariable("id") int id,
            @RequestBody OrderCarts carts) {
        
        OrderCarts result = repository.findOne(id);
        if(result == null) return  new ResponseEntity("Order Cart ID Misses",HttpStatus.GONE); 
        
        if(carts.getProductLinkId()!=null) {
            Products product = productRepository.findOne(carts.getProductLinkId());
           if(product == null) return  new ResponseEntity("Product ID Misses",HttpStatus.GONE); 
            result.setProductId(product);
        }
        
        if(carts.getOrderLinkId()!=null) {
            Orders order = orderRepository.findOne(carts.getOrderLinkId());
           if(order == null) return  new ResponseEntity("Order ID Misses",HttpStatus.GONE); 
            result.setOrderId(order);
        }
        
        if(!isEmpty(carts.getUnitType())) result.setUnitType(carts.getUnitType());
        if(carts.getUnits()!=null) result.setUnits(carts.getUnits());
        if(carts.getPrice()!=null) result.setPrice(carts.getPrice());
        
        repository.save(result); 
        return new ResponseEntity("Carts updated successfully", HttpStatus.ACCEPTED);
    }
    
    @RequestMapping(method = DELETE, value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Delete a Order cart")
    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Delete  Order Cart"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Order cart"),
           })
    public ResponseEntity deleteCart(@PathVariable("id") int id) {
        OrderCarts result = repository.findOne(id);
        
        if(result == null) return  new ResponseEntity("Order Cart ID Misses",HttpStatus.GONE); 
        repository.delete(id);
        return new ResponseEntity("Carts deleted successfully", HttpStatus.ACCEPTED);
    }
}
