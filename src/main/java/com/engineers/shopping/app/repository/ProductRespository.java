package com.engineers.shopping.app.repository;

import com.engineers.shopping.app.model.Products;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Maintains Products
 * @author pandit.summit@gmail.com
 */
@Repository
@Transactional
public interface ProductRespository  extends PagingAndSortingRepository<Products, Integer> { 

  @Query(value = "SELECT prod FROM Products prod WHERE prod.name like  %?1")
  Page<Products> findByName(String name, Pageable pageable);

}
