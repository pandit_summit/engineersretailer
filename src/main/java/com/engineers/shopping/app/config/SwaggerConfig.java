package com.engineers.shopping.app.config;

import static com.google.common.collect.Lists.newArrayList;
import java.util.*;
import org.springframework.context.annotation.*;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.*;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger UI2 Config
 * @author Pandit_Biradar@gensler.com
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.engineers.shopping.app.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET,
                        newArrayList(new ResponseMessageBuilder()
                                .code(500)
                                .message("500 message")
                                .responseModel(new ModelRef("Error"))
                                .build(),
                                new ResponseMessageBuilder()
                                        .code(403)
                                        .message("Forbidden!").build()));
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Engineers Reatiler REST API",
                "Engineers Reatiler REST API for Online Store",
                "1.0",
                "Terms of service",
                new Contact("Pandit Biradar","www.engineers.com", "pandit_biradar@engineers.com"),
                "Apache License Version 2.0",
                "https://www.apache.org/licenses/LICENSE-2.0", new ArrayList<>());
        
    }

}
