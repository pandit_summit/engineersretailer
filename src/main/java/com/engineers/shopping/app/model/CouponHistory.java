package com.engineers.shopping.app.model;

import java.io.*;
import javax.persistence.*;
import javax.xml.bind.annotation.*;
/**
 *
 * @author pandit.summit@gmail.com
 */
@Entity
@Table(name = "coupon_history")
@XmlRootElement
public class CouponHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "coupon_id")
    private Integer couponId;
    @Column(name = "coupon_history")
    private String couponHistory;
    @Column(name = "order_number")
    private String orderNumber;
    @Column(name = "applied_discount_value")
    private String appliedDiscountValue;

    public CouponHistory() {
    }

    public CouponHistory(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public String getCouponHistory() {
        return couponHistory;
    }

    public void setCouponHistory(String couponHistory) {
        this.couponHistory = couponHistory;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getAppliedDiscountValue() {
        return appliedDiscountValue;
    }

    public void setAppliedDiscountValue(String appliedDiscountValue) {
        this.appliedDiscountValue = appliedDiscountValue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (couponId != null ? couponId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CouponHistory)) {
            return false;
        }
        CouponHistory other = (CouponHistory) object;
        if ((this.couponId == null && other.couponId != null) || (this.couponId != null && !this.couponId.equals(other.couponId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.engineers.shopping.model.CouponHistory[ couponId=" + couponId + " ]";
    }

}
