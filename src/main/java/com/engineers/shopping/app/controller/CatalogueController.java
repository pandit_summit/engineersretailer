package com.engineers.shopping.app.controller;

import com.engineers.shopping.app.model.Catalogue;
import com.engineers.shopping.app.repository.CatalogueRepository;

import io.swagger.annotations.*;
import static org.apache.commons.lang.StringUtils.isEmpty;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Maintain Catalogue operation
 * @author Pandit_Biradar@gensler.com
 */
@RestController
@RequestMapping("/catalogues")
@Api(value = "Catalogue")
public class CatalogueController {
     
    @Autowired
    private CatalogueRepository repository;
     
    @RequestMapping(method = GET, produces = "application/json")
    @ApiOperation(value = "View all Catalogue ",response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Iterable<Catalogue> getCatalogues() {
       return repository.findAll(); 
    }
    
    @RequestMapping(method = GET, value = "/{id}", produces = "application/json")
    @ApiOperation(value = "Search a Catalogue with an Catalogue ID",response = Catalogue.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved by id"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public @ResponseBody Catalogue getCatalogue(@PathVariable("id") int id) {
       return repository.findOne(id); 
    }
    
    @RequestMapping(method = POST, produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Create a new Catalogue")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New  Attributes created"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Catalogue"),
           })
    public ResponseEntity createCatalogue(@RequestBody Catalogue catalogue) {
        repository.save(catalogue); 
       
        return new ResponseEntity("catalogue saved successfully", HttpStatus.CREATED);
    }
    
    @RequestMapping(method = PUT, value = "/{id}", produces = "application/json", consumes = "application/json")
    @ApiOperation(value = "Update a Catalogue")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated  Catalogue"),
            @ApiResponse(code = 401, message = "You are not authorized to create the resource"),
            @ApiResponse(code = 410, message = "Missing the Catalogue"),
           })
    public ResponseEntity updateCatalogue(@PathVariable("id") int id,
            @RequestBody Catalogue catalogue) {
        Catalogue result = repository.findOne(id);
        
        if(!isEmpty(catalogue.getName())) result.setName(catalogue.getName());
        if(!isEmpty(catalogue.getStatus())) result.setStatus(catalogue.getStatus());
        
        repository.save(catalogue); 
        return new ResponseEntity("catalogue updated successfully", HttpStatus.ACCEPTED);
    }
     
}
